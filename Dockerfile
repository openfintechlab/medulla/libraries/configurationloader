# 
# Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
# Description: 
# Generic container file for nodejs container
# Arguments:
#  - NODE_VERSION=Node's base container version number (default: 12.16.2-slim)
#  - NODE_ENV=production|development (default: production)
#  - PORT=Express listener port on which traffic will be exposed

ARG NODE_VERSION=12.16.2-slim


#
# Builder container
#
FROM node:${NODE_VERSION} AS builder

# Creating application directory
RUN mkdir /opt/node_app && chown -R node:node /opt/node_app
WORKDIR /opt/node_app

USER node
COPY --chown=node:node  package.json yarn.lock tsconfig.json .snyk ./

RUN yarn install --force && \
      chown -R node:node *

COPY --chown=node:node ./src src/
 
RUN  yarn build && \
      chown -R node:node * && \
      rm -fr ./src        

##
## Main Container
##
FROM node:${NODE_VERSION}

LABEL "authur"="openfintechlab.com" \
      "source-repo"="https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api" \
      "copyright"="Copyright 2020-2022 Openfintechlab, Inc. All rights reserved."

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# default to port 3000 for node
ARG PORT=3000
ENV PORT $PORT 

# Creating application directory
RUN mkdir /opt/node_app && \
      mkdir /opt/node_app/bin && \
      mkdir /oralibs && \ 
      chown -R node:node /oralibs && \
      chown -R node:node /opt/node_app && \
      apt-get update && \
      apt-get install libaio1 -y

WORKDIR /opt/node_app

# Using non-root user shipped with the baseline container
USER node
COPY --chown=node:node  ./libs/instantclient_19_8_lin_x86_64/* /oralibs/
COPY --chown=node:node --from=builder  /opt/node_app/ ./
COPY --chown=node:node  ./docs docs/

ENV PATH /opt/node_app/node_modules/.bin:$PATH
RUN npm config set scripts-prepend-node-path auto && \
      yarn install --force

ENV LD_LIBRARY_PATH /oralibs

# When creating an image, you can bypass the package.json's start command and bake it directly 
# into the image itself. First off this reduces the number of processes running inside of your 
# container. Secondly it causes exit signals such as SIGTERM and SIGINT to be received by the 
# Node.js process instead of npm swallowing them.
CMD [ "node", "./bin/index.js" ]

# References:
# - https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#non-root-user
# - https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
# - https://docs.docker.com/develop/develop-images/multistage-build/ 