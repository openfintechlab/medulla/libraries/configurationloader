/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Main entry point of the solution
 * <b>Description</b>
 * - Root entry level file for bootstarting node js application
 * - Load commadn line arguments 
 * <b>Change Log</b>
 * [14/12/2020]:
 *  - Fetch service details from database rather then from service components
 *  - Remove service calling logic
 * [22/12/2020]:
 * - [MM-36](https://openfintechlab.atlassian.net/browse/MM-36):
 *      * Change strategy of fetching configuration from service-master and service-version
 *      * All data will be fetched from service-configuration and service-endpoint
 *      * **SIDE EFFECTS**
 *          - Inconsistency between service-entity (master) and service configuration
 *          - Inconsistency between service-entity (master) and service endpoints
 * @packageDocumentation
 */
import * as yargs                   	from 'yargs';
import chalk                        	from "chalk";
import util                         	from "util";
import logger                       	from "./utils/Logger";
import RedisUtil                    	from "./utils/RedisUtil";
import AppConfig                    	from './utils/AppConfig';
import OracleDBInteractionUtil      	from './utils/OracleDBInteractionUtil';
import SyncDBController                 from './controller/SyncDB.controller';


export class Bootstart{
    public readonly _RETRIAL_COUNT:number = 3;
    private         _RETRIED:number = 0;
    private readonly _RETRIAL_WAIT_INT_SEC = 15; // 15 seconds

    constructor(){
        this.displayBanner();        
    }
    
    public displayBanner(){
        logger.info(chalk.yellow("-----------------------------------"))
        logger.info(chalk.bold.yellow("Medulla - The Financial Middleware"));
        logger.info(chalk.bold.yellow("Copyright @ Openfintechlab.com"));
        logger.info(chalk.yellow("-----------------------------------"))
        logger.info(chalk.cyan("Starting Application"));
        logger.info(chalk.blue(`Exit code 97:`,`Fatal exception during SyncDB run. `));
        logger.info(chalk.blue(`Exit code 98:`,`Exist command by Redis server.`));
        logger.info(chalk.blue(`Exit code 99:`,`Redis Server down / disconnected`));

    }
    
    /**
     * Start the application process
     */
    public async start(){
        let config:any = new AppConfig().getEnvConfig();    
        logger.debug(`Configuration loaded: ${util.inspect(config,{compact:true,colors:true, depth: null})} `)
        let redisUtil!:RedisUtil;
        const sleep = util.promisify(setTimeout);
        for(this._RETRIED = 0; this._RETRIED <= this._RETRIAL_COUNT; this._RETRIED ++){
            if(!config){                
                logger.error(`Required configuration not found. Exiting the application`);
                return;
            }
            try{
                // Connect to REDIS
                logger.info(`Connecting to the Redis server`);
                redisUtil = new RedisUtil(
                    config.redishost,
                    config.redisport,                    
                    config.redispass
                );
                await redisUtil.connect();
        
                // Connect to the Oracle Database server
                await OracleDBInteractionUtil.initPoolConnections(config.ora_db_userid, config.ora_db_pass, config.ora_db_conn_str )
        
                // Print informative message
                logger.info(`******** Utility for syncing Service Registry ********`)        
                logger.info(`+ Pool interval: ${config.poolinterval} seconds`);        
                // ;Start
                // [MM-36::MFB:20201222]: Changing procedure from `startPoolingAgent()` to `startPoolingController()`
                // As part of the strategy, this service component will depend on the service_configurtion and service_endpoints only
                new SyncDBController(config.poolinterval,redisUtil).startPoolingController();
                // ;END
                break; // Break the loop to prevent execution multiple times      
            }catch(error){            
                logger.error(`Error while BootStraping the solution: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
                if(redisUtil){
                    redisUtil.disConnect();
                }   
                if(this._RETRIED < this._RETRIAL_COUNT){
                    logger.info(`Retrying ${this._RETRIED+1}/${this._RETRIAL_COUNT}`);
                    logger.info(`Waiting for ${this._RETRIAL_WAIT_INT_SEC} seconds`);
                    await sleep(this._RETRIAL_WAIT_INT_SEC*1000); 
                    continue;                    
                }else{
                    logger.error(`Retrial count exhausted. Quiting application`);
                    return;
                }     
            }       
        }
            
    }
}



/** Process Signals */
process.once('exit', (_) => async function() {    
    logger.info("[EXIT] Existing the app");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{        
        process.exit(1);   
    }
});

process.once('SIGINT', async function() {
    logger.info("[SIGINT] Disconnecting all listeners");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{                
        process.exit(1);   
    }
});

process.once('SIGTERM', async function() {
    logger.info("[SIGTERM] Disconnecting all listeners");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{        
        process.exit(1);   
    }
        
});

process.once('SIGHUP', async function() {
    logger.info("[SIGHUP] Disconnecting all listeners");
    try{
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{        
        process.exit(1);   
    }
});
/**END */


/**
 * BootStraping Solution
 */
async function start(){
    let bootstart = new Bootstart();
    await bootstart.start();
} start();