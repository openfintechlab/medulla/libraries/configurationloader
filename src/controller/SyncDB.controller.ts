/**
 * @copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @packageDocumentation
 * Type: Controller for managing service configuraiton syncing procedure
 * <b>Description</b>
 * - Manages syncing of service configuraiton from the database
 * <b>Change Log</b>
 * [14/12/2020]:
 *  - Fetch service details from database rather then from service components
 */
import util                     from "util";
import logger                   from "../utils/Logger";
import RedisUtil                from "../utils/RedisUtil";
import OracleDBInteractionUtil  from "../utils/OracleDBInteractionUtil";
import { v4 as uuidv4 }         from "uuid";

 /**
  * @summary Pushes (one way) service config to `Redis`
  * @description Syncs Service configuration and endpoints with the Redis cache server. This 
  * utility class starts a timer and fetches:
  * - Service Name from `med_service` table
  * - Service Version from `med_service_version` table
  * - Then fetches service configuration from `med_service_config` table
  * - Saves the service configuraiton in Redis cache with key `SRVFG.{Service_name}.{service_version}`
  * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
  * @class
  */
 export default class SyncDBController{

     private readonly _MAX_RECORDS: number = 500;
     private readonly _CFG_CACHE_PREFIX:string = "SRVCFG.";
     private readonly _EP_CACHE_PREFIX:string = "EPCFG.";

    sqlStatements= {
        "SQL001_SRV_MASTER_GETALL": `select RES.*,(SELECT COUNT(*) FROM med_service) as TTL_ROWS from (
            SELECT ROWNUM,srv_id, "NAME", title, group_slug, group_name, imp_tech_uri, source_uri, hostname,port, ctx_root_uri, sdk_doc_uri, api_doc_uri, visibility, "TYPE", lifecycle, discussion_chl_uri, tags, doc_md_file_name, created_on, updated_on, created_by, updated_by, last_ops_id 
            FROM med_service) RES where ROWNUM BETWEEN :V1 and :V2 order by rownum `,
        "SQL002_SRV_VERSION_GETONSRVID" : `SELECT ROWNUM,version_id,srv_id,description,container_reg_uri,container_reg_secid,created_on,updated_on,created_by,updated_by,last_ops_id FROM med_service_versions WHERE srv_id=:v1`,
        "SQL003_SRV_SRV_CONFIG": `SELECT service_id,service_name, version_id, config_key,config_key_desc,config_value,config_type,config_hash,created_on,updated_on,created_by  FROM med_service_config WHERE service_id = :v1 AND version_id = :v2 order by created_on desc`,
        "SQL004_SRV_EP_GETONSRVID": `SELECT version_id,endpoint_id,srv_id,endpoint_uri,http_method,created_on,updated_on,created_by,updated_by,last_ops_id FROM  med_service_endpoints WHERE srv_id=:v0 AND version_id=:v1 ORDER BY updated_on desc`                   
    }

    sqlStatements_MM29 = {
        "SQL001_SRV_CONFIG_GET_ALL": `SELECT service_id, service_name, version_id from med_service_config group by service_id, service_name,version_id`,
        "SQL002_SRV_ENDPOINTS_GET_ALL": `SELECT msc.srv_id as service_id,ms.name as service_name, msc.version_id,ms.hostname,ms.port, ms.CTX_ROOT_URI,ms.api_doc_uri,ms.visibility,ms.type FROM med_service_endpoints msc INNER JOIN med_service ms on msc.srv_id = ms.srv_id GROUP BY msc.srv_id, ms.name, msc.version_id, ms.hostname, ms.port,ms.CTX_ROOT_URI, ms.api_doc_uri,ms.visibility,ms.type`
    }
    

    /**
     * @summary Instantiates Service Configuration DB Controller
     * @constructor
     * @param {number} poolInterval Pooling interval
     * @param {RedisUtil} rediUtil Configuration object     
     */
    constructor (
        private poolInterval: number,        
        private redisUtil:RedisUtil       
    ){}

    /**
     * @summary Starts the pooling function
     * @description
     * Starts pooling procedure(s) for fetching service configurations and service endpoints.
     * This procedure superceeds startPoolingAgent() procedure as this procedure **does not** relies
     * on the service master and service version procedures
     * @see [MM-36](https://openfintechlab.atlassian.net/browse/MM-36)
     * @author 
     * @function
     * 
     */
    public startPoolingController(){
        logger.info(`Starting pooling agent for loading service-configuration with pooling interval`, this.poolInterval);
        let interval = setInterval(async () => {
            let batchID:string = uuidv4();
            logger.info(`***************************************************`)
            logger.info(`Starting batch with id: `, batchID);
            logger.info(`***************************************************`)
            // Step#1: Start syncing configurations indepdendently
            this.syncServiceConfiguration(batchID);            
            // Step#2: Start sycing service endpoints independently
            this.syncServiceEndpoints(batchID);
        }, this.poolInterval * 1000);        
    }
    

    private syncServiceConfiguration(batchID:string){
        return new Promise<any> (async (resolve:any, reject: any) => { 
            logger.info(batchID, `Starting service configuration syncing`);            
            let totalServices:number = 0;
            try{
                let serviceList = await OracleDBInteractionUtil.executeRead(this.sqlStatements_MM29.SQL001_SRV_CONFIG_GET_ALL,[]);            
                totalServices = serviceList.rows.length;
                for(let i:number=0; i< serviceList.rows.length;i++){
                    let serviceRow = serviceList.rows[i];
                    let srvConfigRows = await this.getServiceConfig(serviceRow.SERVICE_ID,serviceRow.VERSION_ID, batchID);
                    if(srvConfigRows.rows.length > 0){
                        let srvConfigJSON = await this.createBussObjForAllConfigs_Slim(srvConfigRows);    
                        let cacheKey = this._CFG_CACHE_PREFIX + serviceRow.SERVICE_NAME + '.' + serviceRow.VERSION_ID;
                        logger.info(batchID,`Storing key:`, cacheKey)
                        logger.debug(batchID,`Storing key: [${cacheKey}] and value: [${JSON.stringify(srvConfigJSON)}]`);
                        await this.redisUtil.set(cacheKey,JSON.stringify(srvConfigJSON)); // Save the content in Cache server                                                
                    }                    
                }
                logger.info('Service SYNC finished', `Total Services Synced:`, totalServices);                    
                resolve(true);
            }catch(error){
                logger.debug(`${util.inspect(error,{compact:true,colors:true, depth: null})}`);
                reject(error);
            }                        
        });
    }

    private syncServiceEndpoints(batchID: string){
        return new Promise<any>(async (resolve:any, reject:any) => {
            logger.info(batchID, `Starting service endpoints syncing`);            
            let totalServices:number = 0;
            try{
                let serviceList = await OracleDBInteractionUtil.executeRead(this.sqlStatements_MM29.SQL002_SRV_ENDPOINTS_GET_ALL,[]);            
                totalServices = serviceList.rows.length;
                for(let i:number=0; i< serviceList.rows.length;i++){
                    let serviceRow = serviceList.rows[i];
                    let srvEndpoints = await this.getServiceEndpoint(serviceRow.SERVICE_ID,serviceRow.VERSION_ID, batchID);                    
                    let servEPObj = this.generateServiceEPBussObj(serviceRow,srvEndpoints, batchID);
                    let epCahceKey = this._EP_CACHE_PREFIX + serviceRow.SERVICE_NAME + '.' + serviceRow.VERSION_ID;
                    await this.redisUtil.set(epCahceKey,JSON.stringify(servEPObj)); // Save the content in Cache server                            
                }
                logger.info('Service Endpoint SYNC finished', `Total Endpointss Synced:`, totalServices);                    
                resolve(true);
            }catch(error){
                logger.debug(`${util.inspect(error,{compact:true,colors:true, depth: null})}`);
                reject(error);
            }
        });
    }

    /**
     * @summary Starts pooling agent
     * @function
     * @deprecated
     */
    public startPoolingAgent(){
        logger.info('DEPRECEATED', 'Use startPoolingController() instead', `Starting pooling agend for loading service-configuration with pooling interval`, this.poolInterval);
        let interval = setInterval(async () => {
            try{
                let batchID:string = uuidv4();
                logger.info(`***************************************************`)
                logger.info(`DEPRECEATED Starting batch with id: `, batchID);
                logger.info(`***************************************************`)
                // Step#1: Get all service-master records from the database server.
                // NOTE: Number of records from `service-master` will be limited to abuse case limits
                const srvMaster = await this.getAllServiceMaster(1,this._MAX_RECORDS,batchID);
                // TODO! Check total number of records (TTL_ROWS) and calculate next iteration accordingly

                // Step#2: For each service (defined in service-master) get all service-version records from the 
                //         database server
                for(let i=0;i<srvMaster.rows.length;i++){
                    let srvMstRow = srvMaster.rows[i];
                    let service_id = srvMstRow.SRV_ID;
                    logger.info(batchID, `Getting service version for service: `, srvMstRow.NAME, '/', service_id);     
                    let servVerRows = await this.getServiceVersion(service_id, batchID);
                    if(servVerRows.rows.length <= 0 ){
                        logger.error(batchID, `Mandatory service version for the service not found`, srvMstRow.NAME, '/', service_id);
                        continue; // go back to the next service-master     
                    }                    
                    // Step#3: For each service-version, fetches ALL service-configuration and creates a business object
                    for(let n=0;n<servVerRows.rows.length;n++){
                        let srvVerRow = servVerRows.rows[n];
                        // Service Configuration
                        let srvConfigRows = await this.getServiceConfig(service_id,srvVerRow.VERSION_ID, batchID);
                        // Generate service config object with format mentioned at EOF
                        if(srvConfigRows.rows.length > 0){
                            let srvConfigJSON = await this.createBussObjForAllConfigs_Slim(srvConfigRows);            
                            // Step#4: Save service-configuration in the REDIS cache server
                            let cacheKey = this._CFG_CACHE_PREFIX + srvMstRow.NAME + '.' + srvVerRow.VERSION_ID;
                            logger.info(batchID,`Storing key:`, cacheKey)
                            logger.debug(batchID,`Storing key: [${cacheKey}] and value: [${JSON.stringify(srvConfigJSON)}]`);
                            await this.redisUtil.set(cacheKey,JSON.stringify(srvConfigJSON)); // Save the content in Cache server                            

                            // Get servie endpoints from the database server
                            let srvEndpoints = await this.getServiceEndpoint(service_id,srvVerRow.VERSION_ID, batchID);
                            if(srvEndpoints.rows.length == 0){
                                logger.error(batchID, `Service endpoints for the service not defined`, srvMstRow.NAME, '/', service_id);
                            }else{
                                let servEPObj = this.generateServiceEPBussObj(srvMstRow,srvEndpoints, batchID);
                                let epCahceKey = this._EP_CACHE_PREFIX + srvMstRow.NAME + '.' + srvVerRow.VERSION_ID;
                                await this.redisUtil.set(epCahceKey,JSON.stringify(servEPObj)); // Save the content in Cache server                            
                            }                            
                        }else{
                            logger.error(batchID, `Configuration for service not found`, srvMstRow.NAME, '/', service_id);
                        }                        
                                                                    
                    }                    

                }                                                                               
            }catch(error){
                logger.error(`Error in pooling agent initiation.`);
                logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})}`);
                clearInterval(interval);  // <-- Exit the interval 

                process.exit(97);
            }
        }, this.poolInterval * 1000);        
                
    }

    /**
     * @summary Get All Service Master records from the database server 
     * @param {number} from From index. e.g. 1
     * @param {number} to To index. e.g. 500
     */
    protected async getAllServiceMaster(from:number, to:number, batchID:string){
        logger.info(batchID,`Fetching service configurtion `, `from`, from, ` to`, to);
        try{
            let binds:any = [from,to];
            let result = await OracleDBInteractionUtil.executeRead(this.sqlStatements.SQL001_SRV_MASTER_GETALL,binds);
            logger.debug(batchID, `Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);
            return result;
        }catch(error){
            logger.error(`Error occured while getting all service master from the database`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            throw error; // This needs to be handled at the calling application level
        }
    }

    /**
     * @summary Fetches service version of the specific service-master
     * @param {string} serviceID Unique service ID to etch the srevice information
     */
    protected async getServiceVersion(serviceID:string, batchID:string){
        logger.info(batchID,`Fetching service version for service:`, serviceID);        
        let binds= [serviceID];
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.sqlStatements.SQL002_SRV_VERSION_GETONSRVID, binds);
            logger.debug(batchID,`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            return result;
        }catch(error){
            logger.error(`Error while getting service version from the database ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            throw error;
        }

    }

    /**
     * @summary Get Service Configuration from the datbase server
     * @param serviceID Unique service identifier of a service
     * @param serviceVersion Service version string of a service
     */
    protected async getServiceConfig(serviceID:string, serviceVersion:string, batchID:string){
        logger.info(batchID,`Get service configuration for service:`, serviceID, ` for version:`, serviceVersion);
        try{
            let binds = [serviceID, serviceVersion];
            let result = await OracleDBInteractionUtil.executeRead(this.sqlStatements.SQL003_SRV_SRV_CONFIG,binds);         
            // let busObj = await this.createBussObjForAllConfigs_Slim(result);            
            // return busObj;
            return result;
        }catch(error){
            logger.error(batchID,`Error occured fetching service configuration from the database`,  `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            throw error;
        }
    }

    /**
     * Converts raw database result to JSON object 
     * @param dbResult Database Result from service config select statement
     */
    protected async createBussObjForAllConfigs_Slim(dbResult:any){
        return new Promise<any> ((resolve:any, _: any) => {                        
            var busObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    
                },
                [dbResult.rows[0].SERVICE_NAME]: {}                
            }

            for(let i:number = 0; i< dbResult.rows.length; i++){            
                busObj[dbResult.rows[0].SERVICE_NAME][dbResult.rows[i].CONFIG_KEY] = dbResult.rows[i].CONFIG_VALUE ;              
            }

            resolve(busObj);
        });
    }
    
    /**
     * Get all service endpoints for a specific service and version
     * @param serviceID Service ID
     * @param versionID Version ID
     * @param batchID Batch ID for logging purpose
     */
    protected async getServiceEndpoint(serviceID:string, versionID:string, batchID:string){        
        logger.info(batchID, `Getting servie endpoints for the service`, serviceID, versionID);
        let binds= [serviceID,versionID];
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.sqlStatements.SQL004_SRV_EP_GETONSRVID, binds);
            logger.debug(`Result: ${typeof result} object:`,  `${util.inspect(result,{compact:true,colors:true, depth: null})}`);                 
            return result;
        }catch(error){
            logger.error(`Error while fetching details from the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            throw error;                        
        }
    }

    /**
     * Generates service endpoint business object which needs to be saved in Cache server
     * @param srvMasterRow Service master db object row
     * @param srvEpDBObj serviec endpoint db object
     * @param batchID Batch ID to tag the procedure with
     */
    protected generateServiceEPBussObj(srvMasterRow:any, srvEpDBObj:any, batchID:string){
        logger.info(batchID, `Creating service endpoint business object`)
        var busObj:any = {
            "ctx_root_uri": "",
            "api_doc_uri" : "",
            "visibility": "",
            "type": "",
            "GET": [],
            "PUT": [],
            "DELETE": [],
            "POST": []
        }
         // ;Mapping: Start
         busObj.hostname       = srvMasterRow.HOSTNAME;
         busObj.port           = srvMasterRow.PORT;
         busObj.ctx_root_uri   = srvMasterRow.CTX_ROOT_URI;
         busObj.api_doc_uri    = srvMasterRow.API_DOC_URI;
         busObj.visibility     = srvMasterRow.VISIBILITY;
         busObj.type           = srvMasterRow.TYPE;
         // Step#5: For each instance of the service-endpoints, save the 'endpoint_uri' local business object
         for(let i=0; i < srvEpDBObj.rows.length ; i++){
            let ep = srvEpDBObj.rows[i];
            busObj[ep.HTTP_METHOD].push(ep.ENDPOINT_URI);
         }         
         // ;Mapping: End
         return busObj;
    }
 }

 /**
  * {
	"metadata": {
		"status": "0000",
		"description": "Success!",
		"responseTime": "2020-12-11T12:15:09.225Z"
	},
	"service-master": {
		"NODE_ENV": "Production",
		"OFL_MED_PORT": "3000",
		"OFL_MED_CONTEXT_ROOT": "/service-master",
		"OFL_MED_SRV_TIMEOUT": "30",
		"OFL_MED_CFGPOOLINT": "10",
		"OFL_MED_SRVCONFIG_URI": "http://configmanager:3006/configmanager/config/",
		"OFL_MED_DB_TYPE": "ORA",
		"OFL_MED_DB_USER": "miscatalog",
		"OFL_MED_DB_PASS": "m!sc@tal0g",
		"OFL_MED_DB_CONNSTR": "192.168.159.132:32010/XE",
		"OFL_MED_DB_MAXPOOL": "10",
		"OFL_MED_DB_MINPOOL": "3",
		"OFL_MED_DB_POOLINC": "1"
	}
}
  */