import SyncDBController         from "../controller/SyncDB.controller";
import OracleDBInteractionUtil  from "../utils/OracleDBInteractionUtil";
import AppConfig                from '../utils/AppConfig';
import logger                   from "../utils/Logger";
import util                     from "util";
import { v4 as uuidv4 }         from "uuid";
import RedisUtil                from "../utils/RedisUtil";

/**
 * Wrapper class on top of SyncDBController to access all private / protected procecures for unit testing
 */
class SyncDBContrllerTest extends SyncDBController{
    
    public async getAllServiceMasterTest(from:number, to:number, batchID:string){        
        return await super.getAllServiceMaster(from,to,batchID);
    }
    public async getServiceVersionTest(serviceID:string, batchID:string){
        return await super.getServiceVersion(serviceID,batchID);
    }
    public async getServiceConfigTest(serviceID:string, serviceVersion:string, batchID: string){
        return await super.getServiceConfig(serviceID,serviceVersion, batchID);
    }
    public async createBussObjForAllConfigs_Slim_Test(dbResult:any){
        return super.createBussObjForAllConfigs_Slim(dbResult);
    }
    public async getServiceEndpoint_Test(serviceID:string, versionID:string, batchID:string){        
        return await super.getServiceEndpoint(serviceID,versionID,batchID);
    }
    public generateServiceEPBussObj_Test(srvMaster:any, srvEp:any, batchID:string){
        return super.generateServiceEPBussObj(srvMaster,srvEp,batchID);
    }
}


describe(`DB Syncing test cases`, () => {
    let config:any;
    let redisUtil!:RedisUtil;

    beforeEach( async() => {
        try{
            config = new AppConfig().getEnvConfig();    
            logger.info(`Configuration loaded: ${util.inspect(config,{compact:true,colors:true, depth: null})} `)        
            // Connect to the Ooracle Database server
            await OracleDBInteractionUtil.initPoolConnections(config.ora_db_userid, config.ora_db_pass, config.ora_db_conn_str )   
            logger.info(`Connected with the database server`);            
            // Connect to the Redis server
            redisUtil = new RedisUtil(
                config.redishost,
                config.redisport,
                config.redispass
            );
            await redisUtil.connect();
            expect(true).toBeTruthy();

        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error file connecting to the database server`);            
        }        
    })

    afterEach(async() => {
        logger.info(`***** Closing database conenction`);
        await OracleDBInteractionUtil.closePoolAndExit();
        logger.info(`***** Closing connection with Redis cache server`);
        await redisUtil.disConnect(false);
    })

    test(`Get all master records from service-master`, async() => {
        // new SyncDBController(config.poolInterval,config).
        let batchID:string = uuidv4();
        try{
            let result = await new SyncDBContrllerTest(config.poolInterval, redisUtil).getAllServiceMasterTest(1,500,batchID);
            expect(result).toBeDefined();
            expect(result.rows.length).toBeGreaterThan(0);
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }        
    })

    test(`Get all service version on service id`, async() => {
        let batchID:string = uuidv4();
        let syncDBController:SyncDBContrllerTest = await new SyncDBContrllerTest(config.poolInterval, redisUtil);
        try{
            let srvMasterResult = await syncDBController.getAllServiceMasterTest(1,500,batchID);     
            expect(srvMasterResult).toBeDefined();
            expect(srvMasterResult.rows.length).toBeGreaterThan(0);
            for(let i=0;i< srvMasterResult.rows.length;i++){
                let srvMasterRow = srvMasterResult.rows[i];
                let result = await syncDBController.getServiceVersionTest(srvMasterRow.SRV_ID, batchID);
                expect(result).toBeDefined();
                expect(result.rows.length).toBeGreaterThan(0);
            }            
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }
    })

    test(`Get all service configuration for the service_id and service_version`, async() => {
        let batchID:string = uuidv4();
        let syncDBController:SyncDBContrllerTest = await new SyncDBContrllerTest(config.poolInterval, redisUtil);
        try{
            let srvMasterResult = await syncDBController.getAllServiceMasterTest(1,500,batchID);     
            expect(srvMasterResult).toBeDefined();
            expect(srvMasterResult.rows.length).toBeGreaterThan(0);
            for(let i=0;i< srvMasterResult.rows.length;i++){
                let srvMasterRow = srvMasterResult.rows[i];
                let srvVerRows = await syncDBController.getServiceVersionTest(srvMasterRow.SRV_ID, batchID);
                expect(srvVerRows).toBeDefined();
                expect(srvVerRows.rows.length).toBeGreaterThan(0);
                for(let n=0;n < srvVerRows.rows.length;n++){
                    let srvVerRow = srvVerRows.rows[n];
                    let srvConfigRows =await syncDBController.getServiceConfigTest(srvMasterRow.SRV_ID,srvVerRow.VERSION_ID,batchID);
                    expect(srvConfigRows).toBeDefined();
                    expect(srvConfigRows.rows.length >= 0).toBeTruthy(); // Since some service will not have configuration defined
                    if(srvConfigRows.rows.length > 0){
                        let resultJSON = await syncDBController.createBussObjForAllConfigs_Slim_Test(srvConfigRows);
                        expect(resultJSON).toBeDefined();
                        expect(resultJSON.metadata.status).toBe('0000');
                        expect(resultJSON[srvMasterRow.NAME]).toBeDefined();
                    }
                }                
            }            
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }
    })

    test(`Get all service configuration for the service_id and service_version and save it in the CACHE`, async() => {
        let batchID:string = uuidv4();
        let syncDBController:SyncDBContrllerTest = await new SyncDBContrllerTest(config.poolInterval, redisUtil);
        try{
            let srvMasterResult = await syncDBController.getAllServiceMasterTest(1,500,batchID);     
            expect(srvMasterResult).toBeDefined();
            expect(srvMasterResult.rows.length).toBeGreaterThan(0);
            for(let i=0;i< srvMasterResult.rows.length;i++){
                let srvMasterRow = srvMasterResult.rows[i];
                let srvVerRows = await syncDBController.getServiceVersionTest(srvMasterRow.SRV_ID, batchID);
                expect(srvVerRows).toBeDefined();
                expect(srvVerRows.rows.length).toBeGreaterThan(0);
                for(let n=0;n < srvVerRows.rows.length;n++){
                    let srvVerRow = srvVerRows.rows[n];
                    let srvConfigRows =await syncDBController.getServiceConfigTest(srvMasterRow.SRV_ID,srvVerRow.VERSION_ID,batchID);
                    expect(srvConfigRows).toBeDefined();
                    expect(srvConfigRows.rows.length >= 0).toBeTruthy(); // Since some service will not have configuration defined
                    if(srvConfigRows.rows.length > 0){
                        let resultJSON = await syncDBController.createBussObjForAllConfigs_Slim_Test(srvConfigRows);
                        expect(resultJSON).toBeDefined();
                        expect(resultJSON.metadata.status).toBe('0000');
                        expect(resultJSON[srvMasterRow.NAME]).toBeDefined();
                        let cacheKey = `TEST.SRVCFG.` + srvMasterRow.NAME + '.' + srvVerRow.VERSION_ID;
                        await redisUtil.set(cacheKey, JSON.stringify(resultJSON));                        
                    }
                }                
            }            
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }
    })

    test(`Get all service endpoints for the service_id and service_version`, async() => {
        let batchID:string = uuidv4();
        let syncDBController:SyncDBContrllerTest = await new SyncDBContrllerTest(config.poolInterval, redisUtil);
        try{
            let srvMasterResult = await syncDBController.getAllServiceMasterTest(1,500,batchID);     
            expect(srvMasterResult).toBeDefined();
            expect(srvMasterResult.rows.length).toBeGreaterThan(0);
            for(let i=0;i< srvMasterResult.rows.length;i++){
                let srvMasterRow = srvMasterResult.rows[i];
                let srvVerRows = await syncDBController.getServiceVersionTest(srvMasterRow.SRV_ID, batchID);
                expect(srvVerRows).toBeDefined();
                expect(srvVerRows.rows.length).toBeGreaterThan(0);
                for(let n=0;n < srvVerRows.rows.length;n++){
                    let srvVerRow = srvVerRows.rows[n];
                    let srvEndpoints = await syncDBController.getServiceEndpoint_Test(srvMasterRow.SRV_ID, srvVerRow.VERSION_ID,batchID);
                    expect(srvEndpoints.rows.length >=0 ).toBeTruthy();
                    let busObj = syncDBController.generateServiceEPBussObj_Test(srvMasterRow,srvEndpoints, batchID);
                    logger.info(`${util.inspect(busObj,{compact:true,colors:true, depth: null})} `);
                    expect(busObj).toBeDefined();
                    expect(busObj.ctx_root_uri).toBeDefined();
                }                
            }            
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }
    })

    test(`Get all service endpoints for the service_id and service_version and save it in the Cache Server`, async() => {
        let batchID:string = uuidv4();
        let syncDBController:SyncDBContrllerTest = await new SyncDBContrllerTest(config.poolInterval, redisUtil);
        try{
            let srvMasterResult = await syncDBController.getAllServiceMasterTest(1,500,batchID);     
            expect(srvMasterResult).toBeDefined();
            expect(srvMasterResult.rows.length).toBeGreaterThan(0);
            for(let i=0;i< srvMasterResult.rows.length;i++){
                let srvMasterRow = srvMasterResult.rows[i];
                let srvVerRows = await syncDBController.getServiceVersionTest(srvMasterRow.SRV_ID, batchID);
                expect(srvVerRows).toBeDefined();
                expect(srvVerRows.rows.length).toBeGreaterThan(0);
                for(let n=0;n < srvVerRows.rows.length;n++){
                    let srvVerRow = srvVerRows.rows[n];
                    let srvEndpoints = await syncDBController.getServiceEndpoint_Test(srvMasterRow.SRV_ID, srvVerRow.VERSION_ID,batchID);
                    expect(srvEndpoints.rows.length >=0 ).toBeTruthy();
                    if(srvEndpoints.rows.length > 0){
                        let busObj = syncDBController.generateServiceEPBussObj_Test(srvMasterRow,srvEndpoints, batchID);                    
                        logger.info(`${util.inspect(busObj,{compact:true,colors:true, depth: null})} `);
                        expect(busObj).toBeDefined();
                        expect(busObj.ctx_root_uri).toBeDefined();
                        let epCahceKey = 'TEST.EPCFG.' + srvMasterRow.NAME + '.' + srvVerRow.VERSION_ID;
                        await redisUtil.set(epCahceKey,JSON.stringify(busObj)); // Save the content in Cache server                            
                    }                                        
                }                
            }            
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }
    })

    test(`Check condition in-case not record found`, async() => {
        let batchID:string = uuidv4();
        let syncDBController:SyncDBContrllerTest = await new SyncDBContrllerTest(config.poolInterval, redisUtil);
        try{
            let srvVerRows = await syncDBController.getServiceVersionTest('', batchID);
            expect(srvVerRows).toBeDefined();
            expect(srvVerRows.rows.length <=0).toBeTruthy()            
        }catch(error){
            logger.error(`${util.inspect(error,{compact:true,colors:true, depth: null})} `);
            fail(`Error while getting service-master`);
        }
    })


});