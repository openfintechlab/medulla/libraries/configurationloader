/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Revamped AppConfig file for loading configurations from environment variable
 * Description:
 * Module for loading configuration from environment variables
 * Library Used: https://github.com/evanshortiss/env-var
 */

 import logger          from "./Logger";
 import * as env        from 'env-var';
 import util            from "util";
 import * as dotenv     from "dotenv";

 export default class AppConfig{
    

    // TODO! CHange the environment variables and remove servicemaster, serviceversion, serviceendpoint and service-config URLs
    public getEnvConfig(): any {
        try{
            // Load configuration from env if process.env.LOAD_FROM_CONFIG is set
            if(process.env.LOAD_FROM_CONFIG){
                let envPath = { path: process.cwd() +`/${process.env.CONFIG_FILE}` };
                logger.info(`Loading configuration from env file:`, envPath );
                dotenv.config(envPath); // loads config file from environment variable
            }
            let config:any = {
                "type": env.get('TYPE').required().asEnum(['service-endpoints', 'service-configuration']),
                // "srvmasterurl": env.get('SRVMASTERURL').required().asUrlString(),
                "ora_db_conn_str": env.get('OFL_MED_DB_CONNSTR').required().asString(),
                "ora_db_userid": env.get('OFL_MED_DB_USER').required().asString(),
                "ora_db_pass": env.get('OFL_MED_DB_PASS').required().asString(),
                "ora_max_pool": env.get('OFL_MED_DB_MAXPOOL').default(10).asIntPositive(),
                "ora_min_pool": env.get('OFL_MED_DB_MINPOOL').default(1).asIntPositive(),
                "ora_pool_inc": env.get('OFL_MED_DB_POOLINC').default(1).asIntPositive(),
                "poolinterval": env.get('POOLINTERVAL').default(30).asIntPositive(),
                // "resptimeout": env.get('RESPTIMEOUT').default(5).asIntPositive(),
                "redishost": env.get('REDISHOST').required().asString(),
                "redisport": env.get('REDISPORT').required().asPortNumber(),
                "redispass": env.get('REDISPASS').asString()
                // "srvverurl": env.get('SRVVERURL').required().asUrlString(),
                // "srvendpointurl": env.get('SRVENDPOINTURL').asUrlString(),
                // "srvconfigurl": env.get('SRVCONFIGURL').asUrlString()
            }
            return config;
        }catch(error){            
            logger.error(`- Error while parsing environmnt variables: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            return false;
        }
    }

}


/**
 * ENV TYPE      
      SRVMASTERURL
      POOLINTERVAL
      RESPTIMEOUT
      REDISHOST
      REDISPORT
      REDISPASS
      SRVVERURL
      SRVENDPOINTURL
 */