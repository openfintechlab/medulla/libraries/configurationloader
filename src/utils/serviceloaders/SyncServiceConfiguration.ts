import logger           from "../Logger";
import axios            from "axios";
import util             from "util";
import chalk            from "chalk";
import RedisUtil        from "../RedisUtil";


export default class SyncServiceConfiguration{
    private readonly _MAX_RETRY_COUNT: number = 5;
    private _retryCount: number = 0;
    private readonly _CACHE_PREFIX:string = "SRVCFG."
    
    constructor(private srvMasterUrl:string, 
        private poolint:number, 
        private resptimeout: number,
        private args: any,
        private redisUtil:RedisUtil){

    }

    public startPoolAgent(){
        logger.info(`Starting pool agent for loading service-configurations`);
        let from:number = 1;
        let to:number = 100;
        let interval = setInterval(async () => {
            try{         
                // Step#1: Get all service-master records       
                const srvMastersResp = await this.getServiceMaster(this.srvMasterUrl,this.resptimeout, from,to);                                       
                // Step#2: For each service, get service version
                await this.getServiceVersionAndServiceConfig(this.args.srvverurl,srvMastersResp.data);
                
            }catch(error){
                const hasResponse = (error.response)? true: false;
                if(hasResponse){
                    logger.error(`Error response received: [Response Code: ${error.response.status}] ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);
                }else{
                    logger.error(`Error response received: ${error.message}`);
                }
                this._retryCount ++;
                if(this._retryCount === this._MAX_RETRY_COUNT){
                    logger.error(`Retry count exhausted: ${this._retryCount}/${this._MAX_RETRY_COUNT}`);
                    logger.error(`Exiting pooling agent`);
                    this.redisUtil.disConnect();
                    clearInterval(interval);  // <-- Exit the interval                    
                }else{
                    logger.info(`Retrying sync ${this._retryCount}/${this._MAX_RETRY_COUNT}`);
                }
            }      

        }, this.poolint* 1000);
    
    }

    private async getServiceMaster(url:string, respTimeout:number, from:number, to:number){        
        try{                        
            url = url + `?from=${from}&to=${to}`;
            logger.debug(`Calling service-master on url: ${url}`);
            const srvMastersResp = await axios.get(url,{
                "timeout": respTimeout * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });            
            logger.info(`Response received with status code: ${chalk.bgGreen(srvMastersResp.status)}`);
            logger.debug(`Response received: ${util.inspect(srvMastersResp.data,{compact:true,colors:true, depth: null})}`)
            let totalRows:number = 0;
            srvMastersResp.data.metadata.trace.forEach( (value:any) => {
                if(value.source === 'TotalRows'){
                    totalRows =  value.description;
                    logger.info(`Total Rows returned for syncing: ${totalRows}`);
                }
            });
            
            return {data: srvMastersResp.data, recordCount: totalRows};
        }catch(error){
            throw error;
        }
    }

    private getServiceVersionAndServiceConfig(srvverurl:string, srvMasterResponse:any){        
        logger.info(`Fetching service version from URL: ${srvverurl}`);
        let count:number = 1;
        try{
            srvMasterResponse["service-master"].forEach(async (serviceMaster:any) => {
                logger.info(`Fetching service version for: (${count++}) ${serviceMaster.name + ':' + serviceMaster.srv_id}`)  
                
                // Get service version for each service-master
                let srvVer = await this.getServiceVersion(srvverurl,serviceMaster.srv_id);
                let meta_status:string = "";
                if(srvVer !== undefined && srvVer.metadata.status === '0000'){
                    meta_status = srvVer.metadata.status;
                    srvVer['service-version'].forEach(async (srvVersion:any) => {
                        // Get service configuration using version id and service name
                        logger.info(`Fetching service configuraiton for service id: ${serviceMaster.srv_id} and srevice-version: ${srvVersion.version_id}`);
                        let srvConfig = await this.getServiceConfig(this.args.srvconfigurl, serviceMaster.srv_id, srvVersion.version_id);
                        // Save the received response in the Cache server
                        let cacheKey = this._CACHE_PREFIX + serviceMaster.name + '.' + srvVersion.version_id;
                        logger.debug(`Storing key: [${cacheKey}] and value: [${JSON.stringify(srvConfig)}]`);
                        await this.redisUtil.set(cacheKey,JSON.stringify(srvConfig));  
                        logger.info(`+ Values Saved in the cache store with key ${cacheKey}`);              
                    });
                }else{
                    logger.info(`- Service version for the specific service [${serviceMaster.srv_id}] is not defined. Error code:  ${meta_status}`)
                }                  
            });
        }catch(error){
            throw error;
        }
       

    }

    private async getServiceVersion(srvverurl:string, srv_id:string){
        logger.info(`Calling service Version API on url: ${srvverurl + '/' + srv_id}`);        
        try{
            logger.debug(`Calling service-version on url: ${srvverurl}`);
            const srvVersions = await axios.get(srvverurl + '/' + srv_id,{
                "timeout": this.resptimeout * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            logger.info(`+ Response received with response code: ${srvVersions.status}`);
            logger.debug(`+ Response message: ${util.inspect(srvVersions.data,{compact:true,colors:true, depth: null})}`);
            return srvVersions.data;        
        }catch(error){
            const hasResponse = (error.response)? true: false;
            if(hasResponse){
                logger.error(`Error response received while fetching service-version: [Response Code: ${error.response.status}]`);
                logger.debug(`Error message: ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);
            }else{
                logger.error(`Error response received  while fetching service-version: ${error.message}`);
            }    
            return;        
        }        

    }

    private async getServiceConfig(srvconfigurl:string, srv_id:string, version_id:string){
        let srvCfgURL = srvconfigurl + '?' + 'serviceID=' + srv_id + '&' + 'versionID=' + version_id;
        logger.info(`Calling service configuration API on url: ${srvCfgURL}`);        
        try{
            
            const srvConfig = await axios.get( srvCfgURL,{
                "timeout": this.resptimeout * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            logger.info(`+ Response received with response code: ${srvConfig.status}`);
            logger.debug(`+ Response message: ${util.inspect(srvConfig.data,{compact:true,colors:true, depth: null})}`);
            return srvConfig.data;        
        }catch(error){
            const hasResponse = (error.response)? true: false;
            if(hasResponse){
                logger.error(`Error response received while fetching service-configuration: [Response Code: ${error.response.status}]`);
                logger.debug(`Error message: ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);
            }else{
                logger.error(`Error response received  while fetching service-configuration: ${error.message}`);
            }    
            return;        
        }        

    }
    
}