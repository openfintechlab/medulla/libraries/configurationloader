import logger           from "../Logger";
import axios            from "axios";
import util             from "util";
import chalk            from "chalk";
import RedisUtil        from "../RedisUtil";

export default class SyncServiceEndpoints{    
    private readonly _MAX_RETRY_COUNT: number = 5;
    private _retryCount: number = 0;
    private readonly _CACHE_PREFIX:string = "EPCFG."
    

    constructor(private srvMasterUrl:string, 
                private poolint:number, 
                private resptimeout: number,
                private args: any,
                private redisUtil:RedisUtil){
        
    }

    /**
     * Starts the pooling agent
     */
    public startPoolAgent(){
        // Step#1: Call service to get all service-master
        // NOTE: By default Service master response is confined with 10 records
        logger.info(`Starting pooling agent for loading service-endpoints`);
        let from:number = 1;
        let to:number = 100;
        // : Variables for calculating number of iterations
        // let iterations: number = 0;
        // let postItRecords: number;
        // let isFirstYeild:boolean = true;
        // : END
        let interval = setInterval(async ()=> {
            logger.info(`Getting all service configuration`);
            try{                
                const srvMastersResp = await this.getServiceMaster(this.srvMasterUrl,this.resptimeout, from,to);                                       
                // Get Service Endpoints for each service Master record and save the endpoint in                 
                await this.getServiceVersionsForEachServiceMaster(this.args.srvverurl,srvMastersResp.data);
                
            }catch(error){
                const hasResponse = (error.response)? true: false;
                if(hasResponse){
                    logger.error(`Error response received: [Response Code: ${error.response.status}] ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);
                }else{
                    logger.error(`Error response received: ${error.message}`);
                }
                this._retryCount ++;
                if(this._retryCount === this._MAX_RETRY_COUNT){
                    logger.error(`Retry count exhausted: ${this._retryCount}/${this._MAX_RETRY_COUNT}`);
                    logger.error(`Exiting pooling agent`);
                    this.redisUtil.disConnect();
                    clearInterval(interval);  // <-- Exit the interval                    
                }else{
                    logger.info(`Retrying sync ${this._retryCount}/${this._MAX_RETRY_COUNT}`);
                }
            }                        
        }, this.poolint * 1000)
    }

    private async getServiceMaster(url:string, respTimeout:number, from:number, to:number){        
        try{                        
            url = url + `?from=${from}&to=${to}`;
            logger.debug(`Calling service-master on url: ${url}`);
            const srvMastersResp = await axios.get(url,{
                "timeout": respTimeout * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });            
            logger.info(`Response received with status code: ${chalk.bgGreen(srvMastersResp.status)}`);
            logger.debug(`Response received: ${util.inspect(srvMastersResp.data,{compact:true,colors:true, depth: null})}`)
            let totalRows:number = 0;
            srvMastersResp.data.metadata.trace.forEach( (value:any) => {
                if(value.source === 'TotalRows'){
                    totalRows =  value.description;
                    logger.info(`Total Rows returned for syncing: ${totalRows}`);
                }
            });
            
            return {data: srvMastersResp.data, recordCount: totalRows};
        }catch(error){
            throw error;
        }
    }

    private async getServiceVersionsForEachServiceMaster(srvverurl:string, srvMaster:any){
        logger.info(`Fetching service version from URL: ${srvverurl}`);
        let count:number = 1;
        try{
            // Step#1: iterate through all instances of service-master and FOREACH service-master, fetch service-version
            srvMaster["service-master"].forEach(async (serviceMaster:any) => {
                logger.info(`Fetching service version for: (${count++}) ${serviceMaster.name + ':' + serviceMaster.srv_id}`)                            
                // Step#2: Fetch ALL service-versions for the service-master
                // **NOTE:** Response will be limited to 10 service versions because of pagination logic.
                //           This can be controlled by providing {cmdargs:srvverurl|env-vars:SRVVERURL}?from=1&to=100 
                let srvVer = await this.getServiceVersion(srvverurl,serviceMaster.srv_id);
                let meta_status:string = "";
                if(srvVer !== undefined && srvVer.metadata.status === '0000'){
                    meta_status = srvVer.metadata.status;
                    // Step#3: Iterate through all the different instances of servie-versions and fetch endpoints details
                    srvVer['service-version'].forEach(async (srvVersion:any) => {
                        // Step#4: Fetch service-endpoints. URL is determeined by cmdargs:srvendpointurl OR env-var:SRVENDPOINTURL
                        let srvEp = await this.getServiceEndpoint(this.args.srvendpointurl,serviceMaster.srv_id,srvVersion.version_id) 
                        let epDetail:any = this.generateBussObj();   
                        // ;Mapping: Start
                        epDetail.hostname       = serviceMaster.hostname;
                        epDetail.port           = serviceMaster.port;
                        epDetail.ctx_root_uri   = serviceMaster.ctx_root_uri;
                        epDetail.api_doc_uri    = serviceMaster.api_doc_uri;
                        epDetail.visibility     = serviceMaster.visibility;
                        epDetail.type           = serviceMaster.type;
                        // Step#5: For each instance of the service-endpoints, save the 'endpoint_uri' local business object
                        srvEp["service-endpoints"].forEach( async (endpoint:any) => {                                  
                            epDetail[endpoint.http_method].push(endpoint.endpoint_uri);
                        });        
                        logger.debug(`Business object: ${util.inspect(epDetail,{compact:true,colors:true, depth: null})}`) 
                        // ;Mapping: End
                        // Step#6: Save local business object to cache server using key: [service-master.name].[service-version.version_id]
                        let cacheKey = this._CACHE_PREFIX + serviceMaster.name + '.' + srvVersion.version_id;                
                        logger.debug(`Storing key: [${cacheKey}] and value: [${JSON.stringify(epDetail)}]`);
                        await this.redisUtil.set(cacheKey,JSON.stringify(epDetail));                
                        logger.info(`+ Values Saved in the cache store with key ${cacheKey}`);
                    });                    
                }else{
                    logger.info(`- Service version for the specific service [${serviceMaster.srv_id}] is not defined. Error code:  ${meta_status}`)
                }              
            });
        }catch(error){
            throw error;
        }        
    }

    private async getServiceVersion(srvverurl:string, srv_id:string){
        logger.info(`Calling service Version API on url: ${srvverurl + '/' + srv_id}`);        
        try{
            logger.debug(`Calling service-version on url: ${srvverurl}`);
            const srvVersions = await axios.get(srvverurl + '/' + srv_id,{
                "timeout": this.resptimeout * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            logger.info(`+ Response received with response code: ${srvVersions.status}`);
            logger.debug(`+ Response message: ${util.inspect(srvVersions.data,{compact:true,colors:true, depth: null})}`);
            return srvVersions.data;        
        }catch(error){
            const hasResponse = (error.response)? true: false;
            if(hasResponse){
                logger.error(`Error response received while fetching service-version: [Response Code: ${error.response.status}]`);
                logger.debug(`Error message: ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);
            }else{
                logger.error(`Error response received  while fetching service-version: ${error.message}`);
            }    
            return;        
        }        

    }

    private async getServiceEndpoint(endpointURL:string,srv_id:string, version_id:string){
        logger.info(`Calling service Endpoint API on url: ${endpointURL + '/' + srv_id + '/' + version_id}`);
        try{
            logger.debug(`Calling service-endpoints on url: ${endpointURL}`);
            const srvEp = await axios.get(endpointURL + '/' + srv_id + '/' + version_id,{
                "timeout": this.resptimeout * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            logger.info(`+ Response received with response code: ${srvEp.status}`);
            logger.debug(`Service Enpoints: ${util.inspect(srvEp,{compact:true,colors:true, depth: null})}`)                    
            return srvEp.data;        
        }catch(error){
            const hasResponse = (error.response)? true: false;
            if(hasResponse){
                logger.error(`Error response received while fetching service-endpoints: [Response Code: ${error.response.status}] `);
                logger.debug(`Error message: ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);

            }else{
                logger.error(`Error response received  while fetching service-endpoints: ${error.message}`);
            }            
        }        

    }

    private generateBussObj(): any{
        return  {
            "ctx_root_uri": "",
            "api_doc_uri" : "",
            "visibility": "",
            "type": "",
            "GET": [],
            "PUT": [],
            "DELETE": [],
            "POST": []
        }
    }

}